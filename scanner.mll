{ open Parser }

let decdigit = ['0'-'9']
let number = decdigit+ ('.' decdigit*)? ('e' '-'? decdigit+)?

rule token =
    parse [' ' '\t' '\r' '\n'] { token lexbuf }
        | ','                  { COMMA }
        | '='                  { EQUAL }
        | '+'                  { PLUS }
        | '-'                  { MINUS }
        | '*'                  { TIMES }
        | '/'                  { DIVIDE }
        | number as lit  { LITERAL(float_of_string lit) }
        | '$' (['0' - '9'] as var) { VARIABLE((int_of_char var) - 48) }
        | eof { EOF }
