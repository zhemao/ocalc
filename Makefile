ocalc: ast.cmx parser.cmx scanner.cmx calc.cmx main.cmx
	ocamlopt $^ -o $@

parser.ml parser.mli: parser.mly
	ocamlyacc $<

scanner.ml: scanner.mll
	ocamllex $<

%.cmx: %.ml
	ocamlopt -c $<

%.cmi: %.mli
	ocamlopt -c $<

clean:
	rm -f *.cm* *.o ocalc
