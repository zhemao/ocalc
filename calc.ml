open Ast

let rec eval variables = function
    Lit(x) -> x
  | Var(i) -> Array.get variables i
  | Seq(e1, e2) ->
        let _ = eval variables e1 in
        eval variables e2
  | Binop(e1, op, e2) ->
        let v1 = eval variables e1 and v2 = eval variables e2 in
        (match op with
            Add -> v1 + v2
          | Sub -> v1 - v2
          | Mul -> v1 * v2
          | Div -> v1 / v2)
  | Assign(var, exp) ->
        let value = eval variables exp in
        let () = (variables.(var) <- value) in value ;;

let eval_and_print str variables =
    let lexbuf = Lexing.from_string str in
    let expr = Parser.expr Scanner.token lexbuf in
    let result = eval variables expr in
    print_endline (string_of_int result);;
