(* main function *)
let () =
    let variables = Array.make 10 0 in
    print_string "> ";
    let input = ref (read_line()) in
    while (!input) <> "quit" do
        Calc.eval_and_print !input variables;
        print_newline();
        print_string "> ";
        input := read_line()
    done ;;
