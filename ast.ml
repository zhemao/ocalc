type operator = Add | Sub | Mul | Div ;;

type expr =
    Binop of expr * operator * expr
  | Seq of expr * expr
  | Assign of int * expr
  | Lit of float
  | Var of int ;;
